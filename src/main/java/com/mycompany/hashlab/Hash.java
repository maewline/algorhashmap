/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hashlab;

import java.rmi.Remote;
import java.util.ArrayList;

/**
 *
 * @author informatics
 */
public class Hash {
    private int key;
    private String value;
    private String[] lookupTable = new String[97]; // assume size = 97  

    public Hash(int key, String value) {
        this.key = key;
        this.value = value;
         lookupTable[hash(key)]=value;
    }

    public void put(int key,String value){ 
       lookupTable[hash(key)]=value;
    }
    
    public int hash(int key){
        return key%lookupTable.length;
    }
    
    public String get(int key){
        return (String) lookupTable[hash(key)];
    }
    public String remove(int index){
        String temp =  lookupTable[hash(index)];
        lookupTable[hash(index)]=null;
        return temp;
    }
}
