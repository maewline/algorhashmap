/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.hashlab;

import java.util.ArrayList;

/**
 *
 * @author Arthi
 */
public class testHash {
    public static void main(String[] args) {
        
        Hash n1 = new Hash(100,"Arthit"); //add values 
        n1.put(501, "Muangsiri");
        n1.put(124, "Good");
        
         //get values from key
        System.out.println(n1.get(501));
        System.out.println(n1.get(100));
        System.out.println(n1.get(124));
        
        System.out.println("\nremove values at key: 100"); //remove values
        System.out.println(n1.remove(100));
        System.out.println(n1.get(100));
        
    }
}

